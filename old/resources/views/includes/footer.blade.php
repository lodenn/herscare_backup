<!--Footer widget area start-->
<section class="footer-widget">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer">
                    <div class="sin-footer-head">
                        <h5>{{trans('main.about_us')}}</h5> </div>
                    <div class="sin-footer-con">
                        <p>{{ str_limit($about->getTranslatedAttribute('description', LaravelLocalization::getCurrentLocale(), 'fallbackLocale'),245)}}</p>

                    </div>
                </div>
            </div>


            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer">
                    <div class="sin-footer-head">
                        <h5>{{trans('main.SUBSCRIBE')}}</h5> </div>
                    <div class="sin-footer-con">
                        <p>{{trans('main.SUBSCRIBE_text')}}</p>
                        <div class="subscribe-link">
                            <input type="text" placeholder="{{trans('main.Your_mail')}}">
                            <button class="mail-send">{{trans('main.SUBSCRIBE')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-social">
                    <ul>
                        <li> <a href="{{$settings->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{{$settings->pinterest}}"><i class="fa fa-pinterest" aria-hidden="true"></i>
                            </a></li>

                        <li><a href="{{$settings->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i>
                            </a></li>


                        <li> <a href="{{$settings->rss}}"><i class="fa fa-instagram" aria-hidden="true"></i>
                            </a> </li>

                        <li><a href="tel:{{$settings->dribbble}}"><i class="fa fa-whatsapp" aria-hidden="true"></i>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer Widget area end-->

<!--footer area start-->
<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p> &copy; 2018 ‐ hers cosmetic All rights reserved. Designed by Shadidsoft </p>
            </div>
        </div>
    </div>
</footer>
<!--footer area end-->