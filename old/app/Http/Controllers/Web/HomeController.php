<?php

namespace App\Http\Controllers\Web;

use App\About;
use App\Category;
use App\ContactUs;
use App\FixedCategory;
use App\Http\Controllers\Controller;
use App\Product;
use App\Slider;
use App\WebsiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Session;

class HomeController extends Controller
{

    public function getProduct($id)
    {
        $product = Product::find($id);
        return view('product',compact('product'));
    }
    public function getCategory($id)
    {

        if($id == 0)
        {
            $products = Product::orderBy('created_at','desc')->paginate(5);

        }else
        {
            $products = Product::where('category_id',$id)->orderBy('created_at','desc')->paginate(5);

        }

        return view('category',compact('products','id','category'));

    }
    public function index()
    {
    	$sliders = Slider::get();
    	$firstSlider = Slider::first();
    	$about = About::first();
    	$categories = Category::get();
        $products = Product::get();

        $fixedCategory = FixedCategory::first();
        
    	$products6mix = Product::orderBy('created_at','desc')->take(6)->get();

        $featureds = Product::where('featured',1)->get();
        
    	return view('index',compact('sliders','firstSlider','about','categories','products','featureds','products6mix','fixedCategory'));
    }

    public function sendContact(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|max:255',
    		'email' => 'required|max:255',
    		'subject' => 'required|max:255',
    		'message' => 'required',
    	]);

    	$contact = new ContactUs;
    	$contact->name = $request->name;
    	$contact->email = $request->email;
    	$contact->subject = $request->subject;
    	$contact->msg = $request->message;
    	$contact->save();

        $email = $contact->email;

        Mail::send('emailContactUs', ['name' => $contact->name, 'email' => $contact->email ,  'subject' => $contact->subject , 'msg' => $contact->msg ], function ($message) use ($email)
        {
            $emailTo = WebsiteSetting::first()->email;
            $message->from($email, $name= null);

            $message->to($emailTo ,'Admin');

            $message->subject("Contact Us Message From Hers WebSite");

        });

    	Session::flash('success-toastr', Lang::get('main.success_contact'));
    	return redirect()->back();
    }
}
