@extends('layouts.website')
@section('content')

    @include('includes.header')
    <!--Main navigation end-->

   <!-- Banner Section -->
	<div id="slider" class="banner carousel slide carousel-fade" data-ride="carousel" data-pause="false">

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">

        @forelse($sliders as $slider)
			<div class="item 
                @if($slider->id == $firstSlider->id)
                    active
                @endif
            " style="background-image:url({{Voyager::image($slider->image)}});">
				<div class="caption-info">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-10 col-md-offset-1">
								<div class="caption-info-inner text-center">
									<h1 class="animated fadeInDown">{{$slider->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</h1>
									<p class="animated fadeInUp">{{$slider->getTranslatedAttribute('desc', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p>
									<a href="{{$slider->link}}" class="animated fadeInUp btn btn-primary page-scroll">{{trans('main.Read_More')}}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        @empty
        @endforelse
			
			
		
		</div><!--end carousel-inner-->
		
		
		<!-- Controls -->
		<a class="control left" href="#slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
		<a class="right control" href="#slider" data-slide="next"><i class="fa fa fa-angle-right"></i></a>
	</div>
	<!--end Banner-->
    
    <!--About us area start-->
    <section id="about" class="about-us area-wrapper">
        <div class="container">
            <div class="row about-inner-wrapper">
                
                <div class="col-md-5">
                    <div class="about-image padding-left">
                        <img src="{{Voyager::image($about->image)}}" alt="">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="area-head about-head">
                        <h2 class="heading">{{trans('main.about_us')}}</h2>
                    </div>
                    <h3 class="about-sub-head">{{$about->getTranslatedAttribute('slugen', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}} </h3>
                    <p>{{$about->getTranslatedAttribute('description', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}} </p>
                   
                  
                </div>
            </div>
        </div>
    </section>  
    <!--About area end-->

    <section class="work area-wrapper Featured-section" style="padding-bottom:0;" >
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="area-head">
                        <h2 class="heading">{{ trans('main.Featured_Products') }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="owl-carousel">

                        @forelse($featureds as $product)
                            <div class="mix " >
                                <div class="mix-wrapper">
                                    <img src="{{Voyager::image($product->image)}}" alt="">
                                    <div class="overlay"></div>
                                    <div class="work-icon">
                                        <div class="work-des"><p>{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p></div>
                                        <a href="{{route('product',$product->id)}}" ><i class="fa fa-link" aria-hidden="true"></i></a>
                                        <a href="{{ Voyager::image($product->image)}}" data-lightbox="image-1" data-title="{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>

                        
                        @empty
                        @endforelse


                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Works area start-->
    <section id="works" class="work area-wrapper">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <div class="area-head">
                        <h2 class="heading">{{trans('main.OUR_PRODUCTS')}}</h2>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="work-filter">
                    <ul class="mixitup-button">


                        <li class="filter active" data-filter=".cat1">{{\App\Category::find($fixedCategory->category_id_1)->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</li>
                        <li class="filter" data-filter=".cat2">{{\App\Category::find($fixedCategory->category_id_2)->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</li>
                        <li class="filter" data-filter=".cat3">{{\App\Category::find($fixedCategory->category_id_3)->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</li>
                        <li class="filter" data-filter=".cat4">{{\App\Category::find($fixedCategory->category_id_4)->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</li>
                    </ul>

                    <div class="mixitup-content" id="mixstart">

                    
                    @forelse(\App\Product::where('category_id',$fixedCategory->category_id_1)->take(6)->get() as $product)


                        <div class="mix cat1 col-xs-12 col-sm-6  col-md-4" >
                           <div class="mix-wrapper">
                                <img src="{{Voyager::image($product->image)}}" alt="">
                                <div class="overlay"></div>
                                <div class="work-icon">
                                   <div class="work-des"><p>{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p></div>
                                    <a href="{{route('product',$product->id)}}" ><i class="fa fa-link" aria-hidden="true"></i></a>
                                    <a href="{{ Voyager::image($product->image)}}" data-lightbox="image-1" data-title="{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div> 
                            </div>    
                        </div>
                    @empty
                    @endforelse


                    @forelse(\App\Product::where('category_id',$fixedCategory->category_id_2)->take(6)->get() as $product)


                        <div class="mix cat2 col-xs-12 col-sm-6  col-md-4" >
                           <div class="mix-wrapper">
                                <img src="{{Voyager::image($product->image)}}" alt="">
                                <div class="overlay"></div>
                                <div class="work-icon">
                                   <div class="work-des"><p>{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p></div>
                                    <a href="{{route('product',$product->id)}}" ><i class="fa fa-link" aria-hidden="true"></i></a>
                                    <a href="{{ Voyager::image($product->image)}}" data-lightbox="image-1" data-title="{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div> 
                            </div>    
                        </div>
                    @empty
                    @endforelse

                    @forelse(\App\Product::where('category_id',$fixedCategory->category_id_3)->take(6)->get() as $product)


                        <div class="mix cat3 col-xs-12 col-sm-6  col-md-4" >
                           <div class="mix-wrapper">
                                <img src="{{Voyager::image($product->image)}}" alt="">
                                <div class="overlay"></div>
                                <div class="work-icon">
                                   <div class="work-des"><p>{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p></div>
                                    <a href="{{route('product',$product->id)}}" ><i class="fa fa-link" aria-hidden="true"></i></a>
                                    <a href="{{ Voyager::image($product->image)}}" data-lightbox="image-1" data-title="{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div> 
                            </div>    
                        </div>
                    @empty
                    @endforelse

                    @forelse(\App\Product::where('category_id',$fixedCategory->category_id_4)->take(6)->get() as $product)


                        <div class="mix cat4 col-xs-12 col-sm-6  col-md-4" >
                           <div class="mix-wrapper">
                                <img src="{{Voyager::image($product->image)}}" alt="">
                                <div class="overlay"></div>
                                <div class="work-icon">
                                   <div class="work-des"><p>{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p></div>
                                    <a href="{{route('product',$product->id)}}" ><i class="fa fa-link" aria-hidden="true"></i></a>
                                    <a href="{{ Voyager::image($product->image)}}" data-lightbox="image-1" data-title="{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div> 
                            </div>    
                        </div>
                    @empty
                    @endforelse


                        

                    </div>

                    <a href="{{ route('category', $categories[0]->id) }}" class="animated zoomIn btn btn-primary page-scroll">{{ trans('main.Read_More')}}</a>
                </div>
            </div>
        </div>
    </section>
    <!--Works area end-->


    <!--contact area start-->
    @include('includes.contact')

@stop