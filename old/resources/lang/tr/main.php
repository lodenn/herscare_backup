<?php

return [

    'Read_More' => 'Daha Fazlası',
    'about_us' => 'hakkımızda',
    'OUR_PRODUCTS' => 'Ürünlerimiz',
    'all' => 'hepsi',
    'Contact_Us' => 'Bize Ulaşın',
    'Phone1' => 'Telefon 1',
    'Phone2' => 'Telefon 2',
    'Email' => 'Eposta',
    'Name' => 'Adı ve Soyadı',
    'Subject' => 'Konu',
    'Message' => 'Mesaj',
    'Send_Message' => 'Mesajı Gönder',
    'success_contact' => 'Mesajınız başarıyla gönderildi',
    'SUBSCRIBE' => 'Abone Ol',
    'SUBSCRIBE_text' => 'Eposta ile bültenimize abone olun ve sitemizdeki haberlerden, özel ürünlerden ve tekliflerden ilk siz haberdar olun',
    'Your_mail' => 'Eposta Adresiniz',
    'home' => 'ana sayfa',
    'PRODUCTS' => 'ÜRÜNLER',
    'contact' => 'iletişim',
    'Language' => 'Dil',
    'Categories' => 'kategoriler',
    'Featured_Products' => 'Özel Ürünler',
    'all_products' => 'Tüm ürünler',

];
