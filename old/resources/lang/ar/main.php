<?php

return [

    'Read_More' => 'اقرا المزيد',
    'about_us' => 'من نحن',
    'OUR_PRODUCTS' => 'منتجاتنا',
    'all' => 'الكل',
    'Contact_Us' => 'تواصل معنا',
    'Phone1' => 'هاتف 1',
    'Phone2' => 'هاتف 2',
    'Email' => 'البريد الالكتروني',
    'Name' => 'الاسم',
    'Subject' => 'الموضوع',
    'Message' => 'الرسالة',
    'Send_Message' => 'ارسال الرسالة',
   	'success_contact' => 'تم ارسال رسالتك بنجاح ',
   	'SUBSCRIBE' => 'اشترك معنا',
   	'SUBSCRIBE_text' => 'اشترك معنا في النشرة البريدية بواسطة بريدك الالكتروني ، ليصلك كل جديد في موقعنا من أخبار ومنتجات مميزة وعروض ',
   	'Your_mail' => 'بريدك الالكتروني',
    'home' => 'الرئيسية',
    'PRODUCTS' => 'المنتجات',
    'contact' => 'التواصل',
    'Language' => 'اللغة',
    'Categories' => 'الاقسام',
    'Featured_Products' => 'منتجات مميزة',
    'all_products' => 'كل منتجاتنا',

];
