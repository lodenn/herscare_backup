<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],
function()
{

Route::get('/','Web\HomeController@index')->name('index');
Route::get('/category/{id}','Web\HomeController@getCategory')->name('category');
Route::get('/product/{id}','Web\HomeController@getProduct')->name('product');
Route::post('/contact','Web\HomeController@sendContact')->name('send.contact');


});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
