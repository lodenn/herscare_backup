<?php

return [

    'Read_More' => 'Read More',
    'about_us' => 'about us',
    'OUR_PRODUCTS' => 'Our Products',
    'all' => 'all',
    'Contact_Us' => 'Contact Us',
    'Phone1' => 'Phone 1',
    'Phone2' => 'Phone 2',
    'Email' => 'Email',
    'Name' => 'Name',
    'Subject' => 'Subject',
    'Message' => 'Message',
    'Send_Message' => 'Send Message',
    'success_contact' => 'Your message was sent successfully',
    'SUBSCRIBE' => 'Subscribe',
    'SUBSCRIBE_text' => 'Subscribe to our newsletter by e-mail, to receive all the news on our site from news and special products and offers',
    'Your_mail' => 'Your E-mail',
    'home' => 'home',
    'PRODUCTS' => 'PRODUCTS',
    'contact' => 'contact',
    'Language' => 'Language',
    'Categories' => 'Categories',
    'Featured_Products' => 'Featured Products',
    'all_products' => 'All Products',

];
