<?php

namespace App\Providers;

use App\About;
use App\Category;
use App\Meta;
use App\WebsiteSetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::share('categoriesHeader' , Category::orderBy('order','desc')->get());
        View::share('settings' , WebsiteSetting::first());
        View::share('about' , About::first());
        View::share('metas' , Meta::get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
