@extends('layouts.website')
@section('content')

@include('includes.header')

<style>
    .breadcrumb::before {
        @if($id != 0)
        background-color: {{ \App\Category::find($id)->background_color }} !important;
        @endif
    }
</style>

	<section class="breadcrumb" style="@if($id != 0) background: url({{ Voyager::image(\App\Category::find($id)->background_image)}}) no-repeat;
    @else background:  url({{ asset('img/banner/10.jpg') }}) no-repeat; @endif     background-position: center;
    	background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>
                        @if($id == 0)
                            {{ trans('main.all_products') }}
                        @else
                    	{{\App\Category::find($id)->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</h2>
                        @endif
                </div>
            </div>
        </div>
    </section>
        
    <!--SINGLE BLOG AREA START-->
    <section class="blog-page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <div class="blog-wrapper">
                      <!--Single Blog -->

                    @forelse($products as $product)
                       <div class="single-blog">
                           <div class="sin-post-image">
                               <img src="{{ Voyager::image($product->image)}}" alt="">
                           </div>
                            <div class="blog-detail">
                               <h3 class="blog-heading"><a href="{{route('product',$product->id)}}">{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</a></h3>
                               <div class="blog-meta">
                                    <span class="published"><i class="fa fa-clock-o"></i>{{$product->created_at->toDateString()}}</span>

                                </div>
                                <div class="blog-content">
                                   <p> {{ str_limit($product->getTranslatedAttribute('description', LaravelLocalization::getCurrentLocale(), 'fallbackLocale') ,600 )}}</p>
                                  <a href="{{route('product',$product->id)}}" class="btn submit-btn btn-primary blog-btn">{{trans('main.Read_More')}}</a>
                                </div>
                            </div>
                       </div>

                    @empty
                    	<div class="single-blog">

                    	</div>
                    @endforelse
                       
                       
                    </div>

                    {{$products->links()}}
                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="sin-widget style-two">
                        <h6 class="mb-20">{{trans('main.Categories')}}</h6>

                        <ul>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="{{ route('category',0)}}">
                                    {{ trans('main.all') }}
                                    ({{\App\Product::get()->count()}})
                                </a>
                            </li>
                        @forelse(\App\Category::orderBy('order','desc')->get() as $cat)
                            <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{ route('category',$cat->id)}}"> {{$cat->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}} ({{\App\Product::where('category_id',$cat->id)->get()->count()}})</a></li>
                        @empty
                        @endforelse
                         
                        </ul>
                    </div>
               </div>

                </div>
            </div>
        </div>
    </section>
        <!--SINGLE BLOG AREA END--> 

    <!--contact area start-->
    @include('includes.contact')

@stop