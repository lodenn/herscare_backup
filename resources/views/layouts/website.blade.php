<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@forelse($metas as $meta){{$meta->name}},@empty @endforelse">

    <title>Hers Cosmetic</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.jpg') }}" type="image/x-icon">
    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <!--Font awesome css-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Bootstrap CSS -->
    <link href="{{url('/')}}/css/bootstrap_{{LaravelLocalization::getCurrentLocaleDirection()}}.min.css" rel="stylesheet">
      <!-- Owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <!-- Animate CSS -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <!-- Normalizer CSS -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
      <!-- Lightbox CSS -->
    <link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{url('/')}}/css/style_{{LaravelLocalization::getCurrentLocale()}}.css">
    <!-- Responsive CSS -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
    <!-- Whatsapp -->
    <link rel="stylesheet" href="{{asset('css/floating-wpp.min.css')}}">
    <!-- Whatsapp -->
    
    <!-- modernizr JS
    ============================================ -->		
    <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body >
    <!-- Whatsapp API -->
    <div class="floating-wpp"></div>
    <!-- Whatsapp API -->

    <!-- Search Box -->
    <div class='search-box'>
        <i class='fa fa-close search-close fa-3x'></i>
        <form action="{{url('/results')}}" method="GET">
            {!! csrf_field() !!}
            <input type="search" name="filter" class='form-control' placeholder="{{ trans('main.search') }}" />
        </form>
    </div>
    <!-- Search Box -->

    @yield('content')
    <!--contact area end-->
    <!--Footer Start-->
    @include('includes.footer')
    <!--End Footer-->
    <!-- jQuery File  -->
   
    <script src="{{ asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mixitup.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.nav.js') }}"></script>
    <script src="{{ asset('js/lightbox.js') }}"></script>
    <script src="{{ asset('js/wow.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('js/ajax-mail.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>		
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>

    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script type="text/javascript">

        @if(Session::has('success-toastr'))
            toastr.success('{{ Session::get('success-toastr') }}')
        @endif

        @if(Session::has('error-toastr'))
            toastr.error('{{ Session::get('error-toastr') }}')
        @endif
        
    </script>

    <!-- Whatsapp -->
    <!--JS-->
    <script src="//code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="{{asset('js/floating-wpp.min.js')}}"></script>
    <!-- Whatsapp -->
    <script type="text/javascript">
          $('.floating-wpp').floatingWhatsApp({
            phone: '+90 544 466 60 65',
            popupMessage: 'Welcome, Send your message ;)',
            showPopup: true,
            position: 'left', // left or right
            autoOpen: false, // true or false
            //autoOpenTimer: 4000,
            message: '',
            //headerColor: 'orange', // enable to change msg box color
            headerTitle: 'Whatsapp Message Box',
            zIndex: 9999,
        });
    </script>
  
  </body>
</html>