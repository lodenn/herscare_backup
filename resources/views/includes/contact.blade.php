<section id="contact" class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-head">
                        <h2 class="heading">{{trans('main.Contact_Us')}}</h2>
                    </div>
                </div>
                <div class="col-sm-5 col-md-4  ">
                    <div class="contact-address contact-wraper">
                       <div class="sin-add">
                          <div class="add-icon">
                            <i class="fa fa-phone" aria-hidden="true"></i>  
                          </div>
						  <p class="phone">{{trans('main.Phone1')}}: <a href="tel:{{$settings->phone1}}">{{$settings->phone1}} </a></p>
						  				 
						  </div>
						   <!-- <div class="sin-add">
                          <div class="add-icon">
                            <i class="fa fa-phone" aria-hidden="true"></i>  
                          </div>
						  <p class="phone">{{trans('main.Phone2')}}: <a href="tel:{{$settings->phone2}}">{{$settings->phone2}}</a></p>
						  				 
						  </div> -->
                       <div class="sin-add">
                          <div class="add-icon">
                             <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                          </div>
                        <p class="mail">
                            {{trans('main.Email')}}:<a href="mailto:{{$settings->email}}">{{$settings->email}}</a>
                        </p>
                       </div>
                        <div class="sin-add">
                          <div class="add-icon">
                              <i class="fa fa-map-marker" aria-hidden="true"></i>
                          </div>
                        <p class="adress">{{$settings->getTranslatedAttribute('address', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p>
                       </div>
                    </div>
                </div>
                <div class="col-sm-7 col-md-8  ">
                    <div class="contact-wraper bg-dark">
                        <form class="contact-form"  action="{{route('send.contact')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-6">
                                <input  placeholder="{{trans('main.Name')}}" name="name" type="text" class='form-control'>  
                            </div>

                            <div class="col-md-6">
                                <input placeholder="{{trans('main.Email')}}" name="email" type="text" class='form-control'>  
                            </div>

                            <div class="col-md-12">
                                <input placeholder="{{trans('main.Subject')}}" name="subject" type="text" class='form-control'>  
                            </div>
                            <div class="col-md-12">
                                <textarea placeholder="{{trans('main.Message')}}" name="message" class='form-control'></textarea>
                            </div>
                            <div class="col-md-12">
                                <button class="btn submit-btn" type="submit" >{{trans('main.Send_Message')}}</button>
                            </div>
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class='map-area'>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d386211.96928799286!2d29.6000778!3d40.8657069!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14caa51e9625440b%3A0x22aca55506fbfe0d!2sHers+cosmetic!5e0!3m2!1sen!2seg!4v1549975904036" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>  
        </div>
        <!-- <div class="map-area">
            <div id="googleMap" ></div>
        </div> -->
    </section>