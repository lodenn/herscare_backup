@extends('layouts.website')
@section('content')

@include('includes.header')



	<section class="breadcrumb" style="background: url({{ asset('img/banner/10.jpg') }}) no-repeat;    background-position: center;
    	background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>
                    	{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</h2>
                </div>
            </div>
        </div>
    </section>



    <section class="blog-page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <div class="blog-wrapper">
                    <!--Single Blog -->
                    <div class="single-blog">
                        <div class="sin-post-image">
                            <img src="{{Voyager::image($product->image)}}" alt="">
                        </div>
                        <div class="blog-detail">
                            <h3 class="blog-heading"><a href="#">{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</a></h3>
                            <div class="blog-meta">
                                <span class="published"><i class="fa fa-clock-o"></i>{{$product->created_at->toDateString()}}</span>
                            </div>
                            <div class="blog-content">
                                <p> {!! nl2br($product->getTranslatedAttribute('description', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="widget-area">
                    <div class="sin-widget style-two">
                        <h6 class="mb-20">{{trans('main.Categories')}}</h6>

                        <ul>
                            <li>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <a href="{{ route('category',0)}}">
                                    {{ trans('main.all') }}
                                    ({{\App\Product::get()->count()}})
                                </a>
                            </li>

                            @forelse(\App\Category::orderBy('order','desc')->get() as $cat)
	                            <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{ route('category',$cat->id)}}"> {{$cat->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}} ({{\App\Product::where('category_id',$cat->id)->get()->count()}})</a></li>
	                        @empty
	                        @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('includes.contact')



@stop