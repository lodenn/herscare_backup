<header class="main-navigation" id="sticker" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#box-nav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="white navbar-brand" href="{{ route('index')}}"><img src="{{asset('img/logow.png')}}" alt=""></a>
                        <a class="dark navbar-brand" href="{{ route('index')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
                    </div>
                    <div class="collapse navbar-collapse" id="box-nav">
                        <ul id="nav" class="nav navbar-nav navbar-right">
                             <li class="current"><a href="{{route('index')}}">{{trans('main.home')}}</a></li>
                            <li><a href="{{ route('index')}}#contact">{{trans('main.contact')}}</a></li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{trans('main.Language')}}
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $key => $value)

                                    <li><a href="{{LaravelLocalization::getLocalizedUrl($key)}}"> {{$value['native']}}</a></li>

                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>