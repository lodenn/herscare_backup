@extends('layouts.website')
@section('content')

@include('includes.header')



	<section class="breadcrumb" style="background: url({{ asset('img/banner/10.jpg') }}) no-repeat;    background-position: center;
    	background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>
                    	{{$title}}
                    </h2>
                </div>
            </div>
        </div>
    </section>



    <section class="blog-page-content">
    <div class="container">
        <div class="row">
        	@if($products->count() > 0)
        	@foreach($products as $product)
            <div class="col-sm-12 col-md-4">
                <div class="blog-wrapper">
                    <!--Single Blog -->
                    <div class="single-blog">
                        <div class="sin-post-image">
                            <img src="{{Voyager::image($product->image)}}" alt="">
                        </div>
                        <div class="blog-detail">
                            <h3 class="blog-heading"><a href="#">{{$product->getTranslatedAttribute('title', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</a></h3>
                            <div class="blog-meta">
                                <span class="published"><i class="fa fa-clock-o"></i>{{$product->created_at->toDateString()}}</span>
                            </div>
                            <div class="blog-content">
                                <p>{{$product->getTranslatedAttribute('description', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <h2>{{trans('main.no_data')}}</h2>
            @endif
        </div>
    </div>
</section>



@stop