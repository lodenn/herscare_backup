<header class="main-navigation" id="sticker" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#box-nav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="white navbar-brand" href="{{ route('index')}}"><img src="{{asset('img/logow.png')}}" alt=""></a>
                        <a class="dark navbar-brand" href="{{ route('index')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
                    </div>
                    <div class="collapse navbar-collapse" id="box-nav">
                        <ul id="nav" class="nav navbar-nav navbar-right">
                            <li><a href="{{ route('index')}}#slider">{{trans('main.home')}}</a>

                            </li>
                            <li><a href="{{ route('index')}}#about">{{trans('main.about_us')}}</a></li>

                            <li><a href="{{ route('index')}}#works">{{trans('main.PRODUCTS')}}</a></li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('main.Categories') }}
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">

                                <li><a href="{{ route('category',0)}}">{{ trans('main.all') }}</a></li>
                                @forelse($categoriesHeader as $cat)
                                    <li><a href="{{ route('category',$cat->id)}}">{{$cat->getTranslatedAttribute('name', LaravelLocalization::getCurrentLocale(), 'fallbackLocale')}}</a></li>
                                @empty
                                @endforelse
                                </ul>
                            </li>


                            <li><a href="{{ route('index')}}#contact">{{trans('main.contact')}}</a></li>
                            <li class="dropdown lang">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    @if(LaravelLocalization::getCurrentLocale() == 'ar')
                                        <img src="{{asset('img/arab.png')}}"> AR
                                    @elseif(LaravelLocalization::getCurrentLocale() == 'en')
                                        <img src="{{asset('img/en.png')}}"> EN
                                    @elseif(LaravelLocalization::getCurrentLocale() == 'tr')
                                        <img src="{{asset('img/turky.png')}}"> TR
                                    @endif
                                    <span class="caret"></span></a>

                                <ul class="dropdown-menu">
                                    

                                    @if(LaravelLocalization::getCurrentLocale() == 'ar')
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('en')}}"> <img src="{{asset('img/en.png')}}" alt=""></a></li>
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('tr')}}"> <img src="{{asset('img/turky.png')}}" alt=""></a></li>
                                    @elseif(LaravelLocalization::getCurrentLocale() == 'en')
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('ar')}}"> <img src="{{asset('img/arab.png')}}" alt=""></a></li>
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('tr')}}"> <img src="{{asset('img/turky.png')}}" alt=""></a></li>
                                    @elseif(LaravelLocalization::getCurrentLocale() == 'tr')
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('ar')}}"> <img src="{{asset('img/arab.png')}}" alt=""></a></li>
                                        <li><a href="{{LaravelLocalization::getLocalizedUrl('en')}}"> <img src="{{asset('img/en.png')}}" alt=""></a></li>
                                    @endif
                                   
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" id='search-btn'><i class='fa fa-search'></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>